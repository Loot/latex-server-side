let http = require('http');
const {parse} = require('querystring');
let mjpage = require('mathjax-node-page').mjpage;
let slashes = require('slashes');

let app = http.createServer(function (req, res) {

    if (req.method === 'POST') {
        let jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {
            contents = parse(jsonString);
            let str = contents.body;
            //str = str.replace(/\r/g, "");
            //str = str.replace(/(?:\\[rn])+/g, "<br />");
            //str = slashes.strip(str);

            mjpage(str, {
                format: ["TeX"],
                //output: '',
                //fragment: true,
                //cssInline: true,
            }, {
                html: true,
                css :false
            }, function (output) {
                res.write(JSON.stringify({'output': output}));
                res.end();
            });
        });
    }
});
app.listen(3001);